package com.cscart.demo.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MySteps {

    protected  WebDriver driver;
    private  WebDriverWait wait;

    @Given("I open the Page with the title $title")
    public void initialize(String title) {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 30);
        driver.get("http://demo.cs-cart.com/");
        wait.until(ExpectedConditions.titleIs(title));
    }

    @When("I search a product with $Batman")
    public void fillProduct(String batman) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search_input")));
        driver.findElement(By.id("search_input")).sendKeys(batman);
        driver.findElement(By.cssSelector("button[title='Search']")).click();
        wait.until(ExpectedConditions.titleIs("Search results"));
    }

    @When("I click in product $product")
    public void clickProduct(String productName){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(productName)));
        driver.findElement(By.linkText(productName)).click();
    }

    @When("I click in 'ADD TO CART'")
    public void clickAddCart() throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("button_cart_94")));
        driver.findElement(By.id("button_cart_94")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[class='ty-btn ty-btn__secondary cm-notification-close ']")));
        driver.findElement(By.cssSelector("a[class='ty-btn ty-btn__secondary cm-notification-close ']")).click();
        Thread.sleep(2000);
    }

    @When("I access my cart")
    public void accessCart() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span[class='ty-minicart-title ty-hand']")));
        driver.findElement(By.cssSelector("span[class='ty-minicart-title ty-hand']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[class='ty-btn ty-btn__secondary']")));
        driver.findElement(By.cssSelector("a[class='ty-btn ty-btn__secondary']")).click();
        wait.until(ExpectedConditions.titleIs("Cart contents"));
    }

    @Then("I check my product added in cart")
    public void assertCart(){
        String product = driver.findElement(By.cssSelector("a[class='ty-cart-content__product-title']")).getText();
        Assert.assertTrue("Product not added", product.equals("Batman: Arkham City (X360)"));
        driver.quit();
    }
}
