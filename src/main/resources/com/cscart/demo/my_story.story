Narrative: Adding a product to the cart

Scenario: I should be able to add a product to the cart.

Given I open the Page with the title Shopping Cart Software & Ecommerce Software Solutions by CS-Cart
When I search a product with Batman
When I click in product Batman: Arkham City (X360)
When I click in 'ADD TO CART'
When I access my cart
Then I check my product added in cart