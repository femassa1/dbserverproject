# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* DBServer Code Project
* Automation Tests 
* [http://demo.cs-cart.com]
* Product : Batman: Arkham City (X360)
* Adding product to cart

### Project Informations ###

* Selenium webdriver
* BDD (Cucumber)
* Maven
* TestNG
* Junit(Assert product)
* Java
* JBehave
* Firefox version: 47.0.1 (Last version suported by firefox for selenium webdriver)

### How to run ? ###

* Right click on file : MyStory.class and "Run" with JUnit

### Who do I talk to? ###

* Repo owner or admin